BEGIN TRANSACTION;

CREATE SEQUENCE public."USER_ID_seq"
INCREMENT 1
    START 35
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;


CREATE TABLE public."USER"
(
    "ID" integer NOT NULL DEFAULT nextval('"USER_ID_seq"'
    ::regclass),
    "EMAIL" character varying
    (254) COLLATE pg_catalog."default" NOT NULL,
    "PASSWORD" character
    (60) COLLATE pg_catalog."default" NOT NULL,
    "ROLE" character varying
    (15) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "USER_pkey" PRIMARY KEY
    ("ID")
);

    COMMIT;
