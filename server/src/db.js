//#region 'NPM MODULES'
const knex = require('knex');
require('dotenv').config();
//#endregion

const db = knex({
  client: 'pg',
  connection: process.env.POSTGRES_URI
});

module.exports = db;
