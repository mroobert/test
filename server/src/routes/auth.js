//#region 'NPM MODULES'
const express = require('express');
const router = express.Router();
const { body } = require('express-validator');
//#endregion

//#region 'PROJ MODULES'
const authController = require('../controllers/authController');
const isAuth = require('../middleware/isAuth');
const isInternal = require('../middleware/isInternal');
const db = require('../db');
//#endregion

const registerValidations = [
  body('email')
    .isEmail()
    .withMessage('Please enter a valid email address.')
    .custom((value) => {
      return db
        .select('*')
        .from('USER')
        .where('EMAIL', value)
        .then((user) => {
          if (user.length) {
            return Promise.reject(
              'E-Mail exists already, please pick a different one.'
            );
          }
        });
    })
    .normalizeEmail(),
  body(
    'password',
    'Please enter a password with only numbers & text and between 8 - 72 characters.'
  )
    .isLength({ min: 8, max: 72 })
    .isAlphanumeric()
    .trim(),
  body('confirmPassword')
    .trim()
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('Passwords have to match!');
      }
      return true;
    })
];

router.post(
  '/register-internal',
  registerValidations,
  authController.registerInternal
);

router.post(
  '/register-external',
  isAuth,
  isInternal,
  registerValidations,
  authController.registerExternal
);

router.post(
  '/log-in',
  [
    body('email')
      .isEmail()
      .withMessage('Email has to be valid')
      .normalizeEmail(),
    body('password', 'Password has to be valid.')
      .isLength({ min: 8, max: 72 })
      .isAlphanumeric()
      .trim()
  ],
  authController.logIn
);

module.exports = router;
