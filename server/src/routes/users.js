//#region 'NPM MODULES'
const express = require('express');
const router = express.Router();
//#endregion

//#region 'PROJ MODULES'
const isAuth = require('../middleware/isAuth');
const isInternal = require('../middleware/isInternal');
const usersController = require('../controllers/usersController');
//#endregion

router.get('/external', isAuth, isInternal, usersController.getExternalUsers);
router.delete('/:userId', isAuth, isInternal, usersController.deleteUser);

module.exports = router;
