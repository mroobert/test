//#region 'NPM MODULES'
const express = require('express');
const router = express.Router();
//#endregion

router.get('/', (req, res, next) => {
  const healthcheck = {
    uptime: process.uptime(),
    message: 'OK',
    timestamp: Date.now()
  };
  try {
    res.status(200).json(healthcheck);
    //throw new Error('error on response');
  } catch (e) {
    healthcheck.message = e;
    next(e);
  }
});

module.exports = router;
