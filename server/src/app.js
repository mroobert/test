//#region 'NPM MODULES'
const express = require('express');
const debug = require('debug')('node-server');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config(); // loading env variables to process.env
//#endregion

//#region 'PROJ MODULES'
const healthcheckRoutes = require('./routes/healthcheck');
const authRoutes = require('./routes/auth');
const usersRoutes = require('./routes/users');
//#endregion

//#region 'INITS'
const app = express();
//#endregion

//#region MIDDLEWARE
app.use(bodyParser.json());
app.use(
  cors({
    origin: 'http://localhost:5000',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
    exposedHeaders: ['Set-Cookie'],
    preflightContinue: false,
    optionsSuccessStatus: 200
  })
);
//#endregion

//#region 'ROUTES'
app.use('/healthcheck', healthcheckRoutes);
app.use(authRoutes);
app.use('/users', usersRoutes);
//#endregion

app.use((error, req, res, next) => {
  const status = error.statusCode || 500;
  const customMessage = error.customMessage || error.message;
  res.status(status).json({ customMessage });
});

const listener = app.listen(process.env.SERVER_PORT, () => {
  debug('node-server listening on ' + listener.address().port);
});
