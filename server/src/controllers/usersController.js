//#region 'PROJ MODULES'
const db = require('../db');
//#endregion

const getExternalUsers = (req, res, next) => {
  db.select('ID', 'EMAIL', 'ROLE')
    .from('USER')
    .where('ROLE', 'external')
    .then((users) => {
      res.json(users);
    })
    .catch((err) => {
      const error = new Error();
      error.customMessage = 'Failed to fetch the users.';
      next(error);
    });
};

const deleteUser = (req, res, next) => {
  const userId = req.params.userId;

  db('USER')
    .where('ID', userId)
    .delete()
    .then((rowsDeleted) => {
      res.json({ rowsDeleted });
    })
    .catch((err) => {
      const error = new Error();
      error.customMessage = 'Failed to delete the user';
      next(error);
    });
};

module.exports = { getExternalUsers, deleteUser };
