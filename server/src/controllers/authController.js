//#region 'NPM MODULES'
const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
//#endregion

//#region 'PROJ MODULES'
const db = require('../db');
//#endregion

const register = (req, res, next, role) => {
  const errors = validationResult(req).array();
  if (errors.length) {
    return res.status(422).json({
      path: '/register-internal',
      customMessage: errors[0].msg
    });
  }

  const email = req.body.email;
  const password = req.body.password;

  bcrypt
    .hash(password, 12)
    .then((hashedPassword) => {
      return db('USER')
        .insert({ EMAIL: email, PASSWORD: hashedPassword, ROLE: role })
        .returning(['ID', 'EMAIL', 'ROLE']);
    })
    .then((user) => {
      res.status(201).json(user[0]);
    })
    .catch((err) => {
      const error = new Error(err);
      error.customMessage = 'Registration failed.';
      next(error);
    });
};

const registerInternal = (req, res, next) => {
  register(req, res, next, 'internal');
};

const registerExternal = (req, res, next) => {
  register(req, res, next, 'external');
};

const logIn = (req, res, next) => {
  const errors = validationResult(req).array();
  if (errors.length) {
    return res.status(422).json({
      path: '/login',
      customMessage: errors[0].msg
    });
  }

  const email = req.body.email;
  const password = req.body.password;

  let authenticatedUser;
  db.select('*')
    .from('USER')
    .where('EMAIL', email)
    .first()
    .then((user) => {
      if (!user) {
        const error = new Error();
        error.statusCode = 401;
        error.customMessage = 'Authentication failed!';
        throw error;
      }
      authenticatedUser = user;
      return bcrypt.compare(password, user.PASSWORD);
    })
    .then((isAuth) => {
      if (!isAuth) {
        const err = new Error();
        err.statusCode = 401;
        error.customMessage = 'Authentication failed!';
        throw err;
      }
      const token = jwt.sign(
        {
          email: authenticatedUser.EMAIL,
          role: authenticatedUser.ROLE
        },
        process.env.ACCESS_TOKEN_SECRET,
        { expiresIn: '15min' }
      );
      res.status(200).json({
        token: token
      });
    })
    .catch((err) => {
      if (err.statusCode) {
        next(err);
      } else {
        const error = new Error();
        error.customMessage = 'Authentication failed!';
        next(error);
      }
    });
};

module.exports = { registerInternal, registerExternal, logIn };
