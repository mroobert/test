module.exports = (req, res, next) => {
  if (req.role !== 'internal') {
    const error = new Error();
    error.customMessage = 'User not-authorized!';
    error.statusCode = 403;
    throw error;
  }
  next();
};
