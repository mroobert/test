//#region 'NPM MODULES'
import jwt from 'jsonwebtoken';
//#endregion

//#region 'PROJ MODULES'
import axios from './axios';
import setAuthorizationToken from './apiUtils';
//#endregion

function setError(error) {
  const data = error.response.data;
  const customMessage = data.customMessage ? data.customMessage : null;
  let err = new Error();
  err.customMessage = customMessage;

  return err;
}

export function logIn(user) {
  return axios
    .post('/log-in', user)
    .then((res) => {
      if (res.status === 200) {
        const token = res.data.token;
        localStorage.setItem('token', token);
        setAuthorizationToken(token);
        return jwt.decode(token);
      }
      return null;
    })
    .catch((err) => {
      throw setError(err);
    });
}

export function registerInternal(user) {
  return axios
    .post('/register-internal', user)
    .then((res) => {
      if (res.status === 201) {
        return true;
      }
      return false;
    })
    .catch((err) => {
      throw setError(err);
    });
}

export function registerExternal(user) {
  return axios
    .post('/register-external', user)
    .then((res) => {
      if (res.status === 201) {
        return res.data;
      }
      return null;
    })
    .catch((err) => {
      throw setError(err);
    });
}

export function getUsers() {
  return axios
    .get('/users/external')
    .then((res) => {
      if (res.status === 200) {
        if (res.data) {
          return res.data;
        }
      }
      return null;
    })
    .catch((err) => {
      throw setError(err);
    });
}

export function deleteUser(userId) {
  return axios
    .delete('/users/' + userId)
    .then((res) => {
      if (res.status === 200) {
        if (res.data) {
          return res.data;
        }
      }
      return null;
    })
    .catch((err) => {
      throw setError(err);
    });
}
