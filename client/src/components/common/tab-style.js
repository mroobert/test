import { makeStyles } from '@material-ui/core/styles';

const useStylesTab = makeStyles({
  tabItemTextColor: {
    color: 'white'
  }
});

export default useStylesTab;
