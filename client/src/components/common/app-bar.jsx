//#region 'NPM DEP'
import React from 'react';
import { connect } from 'react-redux';
import AppBarMui from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import LockOpenIcon from '@material-ui/icons/LockOpen';
//#endregion

//#region 'LOCAL DEP'
import useStylesAppBar from './app-bar-style';
import { logOutUser } from '../../redux/actions/user-action';
import TabButtons from './tab';
//#endregion

function AppBar({ logOutUser, role }) {
  const classes = useStylesAppBar();
  let position = 0;
  if (role === 'internal') {
    position = 2;
  }
  const [value, setValue] = React.useState(position);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  function handleLogOut() {
    localStorage.removeItem('token');
    logOutUser();
  }

  let buttons;
  const tabItems = [
    { id: 1, label: 'About 1', to: '/about' },
    { id: 2, label: 'About 2', to: '/about-secondary' }
  ];
  if (role === 'internal') {
    tabItems.push({ id: 3, label: 'Administration', to: '' });
  }
  buttons = (
    <TabButtons tabItems={tabItems} value={value} handleChange={handleChange} />
  );

  return (
    <span className={classes.root}>
      <AppBarMui color='primary'>
        <Toolbar>
          <div className={classes.buttons}>{buttons}</div>
          <span className={classes.logOut}>
            <Tooltip title='Log out'>
              <IconButton color='inherit' onClick={handleLogOut}>
                <LockOpenIcon />
              </IconButton>
            </Tooltip>
          </span>
        </Toolbar>
      </AppBarMui>
      <Toolbar />
    </span>
  );
}

function mapStateToProps(state) {
  return {
    role: state.user.email ? state.user.role : null
  };
}

const mapDispatchToProps = {
  logOutUser
};

export default connect(mapStateToProps, mapDispatchToProps)(AppBar);
