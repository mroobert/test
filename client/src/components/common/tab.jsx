//#region 'NPM DEP'
import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import { Link } from 'react-router-dom';
import Tab from '@material-ui/core/Tab';
//#endregion

import useStylesAppBar from './tab-style';

function TabButtons({ tabItems, value, handleChange }) {
  const classes = useStylesAppBar();
  return (
    <Tabs
      value={value}
      indicatorColor='secondary'
      className={classes.tabItemTextColor}
      onChange={handleChange}
      centered
    >
      {tabItems.map((tabItem) => {
        return (
          <Tab
            key={tabItem.id}
            component={Link}
            label={tabItem.label}
            to={tabItem.to}
          ></Tab>
        );
      })}
    </Tabs>
  );
}

export default TabButtons;
