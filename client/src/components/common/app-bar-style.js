import { makeStyles } from '@material-ui/core/styles';

const useStylesAppBar = makeStyles({
  logOut: {
    marginLeft: 'auto'
  },
  buttons: {
    marginRight: '100px',
    marginLeft: '30px'
  }
});

export default useStylesAppBar;
