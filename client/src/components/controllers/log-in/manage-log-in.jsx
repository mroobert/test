//#region 'NPM DEP'
import React, { useState } from 'react';
import { useSnackbar } from 'notistack';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
//#endregion

//#region 'LOCAL DEP'
import LogInForm from '../../views/log-in';
import { registerInternal } from '../../../api/api';
import { logIn } from '../../../redux/actions/user-action';
//#endregion

function ManageLogIn({ logIn, authUser }) {
  const registerPage = {
    title: 'Register',
    button: 'Register',
    switch: 'Log In',
    confirmPassword: 'visible'
  };

  const loginPage = {
    title: 'Log In',
    button: 'Log In',
    switch: 'Register',
    confirmPassword: 'hidden'
  };

  const emptyUser = {
    email: '',
    password: '',
    confirmPassword: ''
  };
  //#region HOOKS
  const [user, setUser] = useState(emptyUser);
  const [validations, setValidations] = useState({});
  const [page, setPage] = useState(loginPage);
  const { enqueueSnackbar } = useSnackbar();
  //#endregion

  //#region HANDLERS
  function handleChange(event) {
    const { name, value } = event.target;
    setUser((prevUser) => ({
      ...prevUser,
      [name]: value
    }));
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (!formIsValid()) return;
    if (page.title === 'Register') {
      registerInternal(user)
        .then((result) => {
          if (result) {
            setPage(loginPage);
            setUser(emptyUser);
            enqueueSnackbar('You have successfully registered', {
              variant: 'success',
              anchorOrigin: {
                vertical: 'top',
                horizontal: 'center'
              }
            });
          }
        })
        .catch((error) => {
          showErrorMessage(error);
        });
    } else {
      logIn(user).catch((error) => {
        showErrorMessage(error);
      });
    }
  }

  function showErrorMessage(error) {
    let message = error.customMessage ? error.customMessage : 'Server error';
    enqueueSnackbar(message, {
      variant: 'error',
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'center'
      }
    });
  }
  function handleSwitch() {
    if (page.switch === 'Register') {
      setPage(registerPage);
    } else {
      setUser((prevUser) => ({
        ...prevUser,
        ['confirmPassword']: ''
      }));
      setPage(loginPage);
    }
  }
  //#endregion

  function formIsValid() {
    const { email, password, confirmPassword } = user;
    const validations = {};

    if (!email) {
      validations.email = 'Empty email';
    }
    if (!password) {
      validations.password = 'Empty password';
    }
    if (page.title === 'Register' && !confirmPassword) {
      validations.confirmPassword = 'Empty confirm password';
    }
    setValidations(validations);
    // Form is valid if the errors object still has no properties
    return Object.keys(validations).length === 0;
  }

  const logInForm = (
    <LogInForm
      onChange={handleChange}
      onSubmit={handleSubmit}
      onSwitch={handleSwitch}
      user={user}
      page={page}
      errors={validations}
    ></LogInForm>
  );

  let redirect = logInForm;
  if (authUser) {
    if (authUser.role === 'internal') {
      redirect = <Redirect to='/users-administration' />;
    } else {
      redirect = <Redirect to='/about' />;
    }
  }

  return redirect;
}

function mapStateToProps(state) {
  return {
    authUser: state.user.email ? state.user : null
  };
}

const mapDispatchToProps = {
  logIn
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageLogIn);
