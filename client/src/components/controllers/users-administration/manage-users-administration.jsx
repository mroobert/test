//#region 'NPM DEP'
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useSnackbar } from 'notistack';
//#endregion

//#region 'LOCAL DEP'
import {
  loadUsers,
  deleteExternalUser,
  createExternalUser
} from '../../../redux/actions/users-action';
import UsersCards from '../../views/users-administration/users-cards';
import UserRegister from '../../views/users-administration/user-register';
//#endregion

function ManageUsersAdministration({
  users,
  loadUsers,
  deleteExternalUser,
  createExternalUser
}) {
  const emptyUser = {
    email: '',
    password: '',
    confirmPassword: ''
  };
  //#region HOOKS
  const [user, setUser] = useState(emptyUser);
  const [validations, setValidations] = useState({});
  useEffect(() => {
    if (!users.length) {
      loadUsers().catch((error) => {
        console.log(error.customMessage);
      });
    }
  }, []);

  const { enqueueSnackbar } = useSnackbar();
  //#endregion

  //#region HANDLERS
  function handleChange(event) {
    const { name, value } = event.target;
    setUser((prevUser) => ({
      ...prevUser,
      [name]: value
    }));
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (!formIsValid()) return;

    createExternalUser(user)
      .then((result) => {
        if (result) {
          setUser(emptyUser);
          enqueueSnackbar('User created.', {
            variant: 'success',
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'center'
            }
          });
        }
      })
      .catch((error) => {
        showErrorMessage(error);
      });
  }

  function handleDelete(userId) {
    deleteExternalUser(userId)
      .then((res) => {
        let message = res ? 'User deleted.' : 'Operation failed..';

        enqueueSnackbar(message, {
          variant: 'success',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'center'
          }
        });
      })
      .catch((error) => {
        showErrorMessage(error);
      });
  }
  //#endregion

  function formIsValid() {
    const { email, password, confirmPassword } = user;
    const validations = {};

    if (!email) {
      validations.email = 'Empty email';
    }
    if (!password) {
      validations.password = 'Empty password';
    }
    if (!confirmPassword) {
      validations.confirmPassword = 'Empty confirm password';
    }
    setValidations(validations);
    // Form is valid if the errors object still has no properties
    return Object.keys(validations).length === 0;
  }

  function showErrorMessage(error) {
    let message = error.customMessage ? error.customMessage : 'Server error';
    enqueueSnackbar(message, {
      variant: 'error',
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'center'
      }
    });
  }

  return (
    <>
      <UserRegister
        onSubmit={handleSubmit}
        onChange={handleChange}
        user={user}
        errors={validations}
      />
      <UsersCards users={users} onDelete={handleDelete} />
    </>
  );
}

function mapStateToProps(state) {
  return {
    users: state.users
  };
}

const mapDispatchToProps = {
  loadUsers,
  deleteExternalUser,
  createExternalUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageUsersAdministration);
