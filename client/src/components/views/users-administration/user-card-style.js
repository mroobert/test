import { makeStyles } from '@material-ui/core/styles';

const useStylesUserCard = makeStyles({
  root: {
    maxWidth: 190,
    maxHeigth: 100
  },
  title: {
    fontSize: 14
  },
  email: {
    fontSize: 12
  },
  pos: {
    marginBottom: 12
  }
});

export default useStylesUserCard;
