import { makeStyles } from '@material-ui/core/styles';

const useStylesUserRegister = makeStyles((theme) => ({
  bigContainer: {
    boxShadow: '4px 4px 8px 0px rgba(0,0,0,.2)',
    borderRadius: '.5rem',
    height: '400',
    background: '#FFFFFF',
    marginTop: theme.spacing(4)
  },
  smallContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submitBtn: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default useStylesUserRegister;
