import { makeStyles } from '@material-ui/core/styles';

const useStylesUsersCards = makeStyles({
  container: {
    marginTop: 10,
    padding: 20
  }
});

export default useStylesUsersCards;
