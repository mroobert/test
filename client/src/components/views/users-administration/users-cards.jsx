//#region 'NPM MODULES'
import React from 'react';
import { Grid } from '@material-ui/core';
//#endregion

//#region 'PROJ MODULES'
import UserCard from './user-card';
import useStylesUsersCards from './users-cards-style';
//#endregion

const UserCards = ({ users, onDelete }) => {
  const classes = useStylesUsersCards();
  return (
    <div className={classes.container}>
      <Grid
        container
        spacing={4}
        direction='row'
        justify='center'
        alignItems='center'
      >
        {users.map((user) => (
          <Grid item key={user.ID}>
            <UserCard email={user.EMAIL} onDelete={() => onDelete(user.ID)} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default UserCards;
