//#region 'NPM DEP'
import React from 'react';
// material-ui
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
//#endregion

//#region 'LOCAL DEP'
import useStylesUserRegister from './user-register-style';
//#endregion

function UserRegister({ onSubmit, onChange, user, errors }) {
  const classes = useStylesUserRegister();
  return (
    <Container component='div' maxWidth='sm' className={classes.bigContainer}>
      <Container
        component='div'
        maxWidth='xs'
        className={classes.smallContainer}
      >
        <Typography component='h1' variant='h5'>
          Add external
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <TextField
            variant='outlined'
            margin='normal'
            fullWidth
            label='Email'
            name='email'
            autoFocus
            value={user.email}
            onChange={onChange}
            error={errors.email ? true : false}
            helperText={errors.email}
          />
          <TextField
            variant='outlined'
            margin='normal'
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            autoComplete='new-password'
            value={user.password}
            onChange={onChange}
            error={errors.password ? true : false}
            helperText={errors.password}
          />
          <TextField
            variant='outlined'
            margin='normal'
            fullWidth
            name='confirmPassword'
            label='Confirm password'
            type='password'
            id='confirmPassword'
            autoComplete='new-password'
            value={user.confirmPassword}
            onChange={onChange}
            error={errors.confirmPassword ? true : false}
            helperText={errors.confirmPassword}
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submitBtn}
          >
            Add
          </Button>
        </form>
      </Container>
    </Container>
  );
}

export default UserRegister;
