//#region 'NPM MODULES'
import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
//#endregion

//#region 'PROJ MODULES'
import useStylesUserCard from './user-card-style';
//#endregion

function UserCard({ email, onDelete }) {
  const classes = useStylesUserCard();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography
          className={classes.title}
          color='textSecondary'
          gutterBottom
        >
          User
        </Typography>
        <Typography className={classes.email}>{email}</Typography>
      </CardContent>
      <CardActions>
        <Button size='small' onClick={onDelete}>
          Delete
        </Button>
      </CardActions>
    </Card>
  );
}

export default UserCard;
