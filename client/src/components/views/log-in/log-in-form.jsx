//#region 'NPM DEP'
import React from 'react';
// material-ui
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import LockIcon from '@material-ui/icons/Lock';
//#endregion

//#region 'LOCAL DEP'
import useStylesLogInForm from './log-in-form-style';
//#endregion

function LogInForm({ onSubmit, onChange, onSwitch, user, page, errors }) {
  const classes = useStylesLogInForm();
  return (
    <Container component='div' maxWidth='lg' className={classes.bigContainer}>
      <Container
        component='div'
        maxWidth='xs'
        className={classes.smallContainer}
      >
        <LockIcon className={classes.logo} color='secondary' />

        <Typography component='h1' variant='h5'>
          {page.title}
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <TextField
            variant='outlined'
            margin='normal'
            fullWidth
            label='Email'
            name='email'
            autoFocus
            value={user.email}
            onChange={onChange}
            error={errors.email ? true : false}
            helperText={errors.email}
          />
          <TextField
            variant='outlined'
            margin='normal'
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            autoComplete='new-password'
            value={user.password}
            onChange={onChange}
            error={errors.password ? true : false}
            helperText={errors.password}
          />
          <TextField
            style={{ visibility: page.confirmPassword }}
            variant='outlined'
            margin='normal'
            fullWidth
            name='confirmPassword'
            label='Confirm password'
            type='password'
            id='confirmPassword'
            autoComplete='new-password'
            value={user.confirmPassword}
            onChange={onChange}
            error={errors.confirmPassword ? true : false}
            helperText={errors.confirmPassword}
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submitBtn}
          >
            {page.button}
          </Button>
          <Link href='#' variant='body2' onClick={onSwitch}>
            {page.switch}
          </Link>
        </form>
      </Container>
    </Container>
  );
}

export default LogInForm;
