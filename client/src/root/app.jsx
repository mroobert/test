//#region 'NPM DEP'
import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
//#endregion

//#region 'LOCAL DEP'
import theme from './app-style';
import ManageLogIn from '../components/controllers/log-in/';
import ManageUsersAdministration from '../components/controllers/users-administration';
import About from '../components/views/about';
import AboutSecondary from '../components/views/about-secondary';
import AppBar from '../components/common/app-bar';
//#endregion

function App({ authUser }) {
  let routes = (
    <Switch>
      <Route exact path='/' component={ManageLogIn} />
      <Redirect to='/' />
    </Switch>
  );

  if (authUser) {
    if (authUser.role === 'internal') {
      routes = (
        <>
          <AppBar />
          <Switch>
            <Route exact path='/' component={ManageLogIn} />
            <Route
              path='/users-administration'
              component={ManageUsersAdministration}
            />
            <Route path='/about' component={About} />
            <Route path='/about-secondary' component={AboutSecondary} />
          </Switch>
        </>
      );
    } else {
      routes = (
        <>
          <AppBar />
          <Switch>
            <Route exact path='/' component={ManageLogIn} />
            <Route path='/about' component={About} />
            <Route path='/about-secondary' component={AboutSecondary} />
          </Switch>
        </>
      );
    }
  }

  return (
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider>{routes}</SnackbarProvider>
    </MuiThemeProvider>
  );
}

function mapStateToProps(state) {
  return {
    authUser: state.user.email ? state.user : null
  };
}

export default connect(mapStateToProps, null)(App);
