//#region 'NPM MODULES'
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';
import setAuthorizationToken from './api/apiUtils';
import jwt from 'jsonwebtoken';
//#endregion

//#region 'PROJ MODULES
import './index.css';
import App from './root/app';
import axiosInstance from './api/axios';
import configureStore from './redux/configure-store';
import { setCurrentUser } from './redux/actions/user-action';
//#endregion

let apiDown = (
  <p style={{ textAlign: 'center', fontSize: '30px' }}>API down.</p>
);

axiosInstance
  .get('/healthcheck')
  .then((response) => {
    if (response.status === 200) {
      if (response.data.message === 'OK') {
        const store = configureStore();
        const token = localStorage.getItem('token');
        if (token) {
          setAuthorizationToken(token);
          store.dispatch(setCurrentUser(jwt.decode(token)));
        }
        return (
          <ReduxProvider store={store}>
            <Router>
              <App />
            </Router>
          </ReduxProvider>
        );
      }
    } else {
      return apiDown;
    }
  })
  .then((result) => {
    ReactDOM.render(result, document.getElementById('app'));
  })
  .catch((error) => {
    ReactDOM.render(apiDown, document.getElementById('app'));
  });
