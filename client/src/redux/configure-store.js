//#region 'PROJ MODULES'
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
//#endregion

//#region 'PROJ MODULES'
import appReducer from './reducers';
import { LOG_OUT_USER } from './actions/action-types';
//#endregion

const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state
  if (action.type === LOG_OUT_USER) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default function configureStore(initialState) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //add support for redux dev tools
  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(thunk))
  );
}
