import * as actionTypes from './action-types';
import { getUsers, deleteUser, registerExternal } from '../../api/api';

function loadUsersSuccess(users) {
  return {
    type: actionTypes.LOAD_USERS_SUCCESS,
    users
  };
}

function deleteExternalUserSuccess(userId) {
  return {
    type: actionTypes.DELETE_USER_SUCCESS,
    userId
  };
}

function createExternalUserSuccess(user) {
  return {
    type: actionTypes.CREATE_EXTERNAL_USER_SUCCESS,
    user
  };
}

export function loadUsers() {
  return function(dispatch) {
    return getUsers()
      .then((users) => {
        dispatch(loadUsersSuccess(users));
      })
      .catch((error) => {
        throw error;
      });
  };
}

export function deleteExternalUser(userId) {
  return function(dispatch) {
    return deleteUser(userId)
      .then((res) => {
        if (res) {
          dispatch(deleteExternalUserSuccess(userId));
          return true;
        }
        return false;
      })
      .catch((error) => {
        throw error;
      });
  };
}

export function createExternalUser(user) {
  return function(dispatch) {
    return registerExternal(user)
      .then((newUser) => {
        if (newUser) {
          dispatch(createExternalUserSuccess(newUser));
          return true;
        }
        return false;
      })
      .catch((error) => {
        throw error;
      });
  };
}
