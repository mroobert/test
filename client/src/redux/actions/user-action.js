import * as actionTypes from './action-types';
import * as api from '../../api/api';

function logInSuccess(user) {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    user
  };
}

export function setCurrentUser(user) {
  return {
    type: actionTypes.SET_CURRENT_USER,
    user
  };
}

export function logOutUser() {
  return {
    type: actionTypes.LOG_OUT_USER
  };
}

export function logIn(user) {
  return function(dispatch) {
    return api
      .logIn(user)
      .then((loggedUser) => {
        dispatch(logInSuccess(loggedUser));
      })
      .catch((error) => {
        console.log(error);
        throw error;
      });
  };
}
