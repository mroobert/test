import * as actionTypes from '../actions/action-types';
import initialState from './initial-state';

export default function userReducer(state = initialState.users, action) {
  switch (action.type) {
    case actionTypes.LOAD_USERS_SUCCESS:
      return action.users;
    case actionTypes.DELETE_USER_SUCCESS:
      return state.filter((u) => u.ID !== action.userId);
    case actionTypes.CREATE_EXTERNAL_USER_SUCCESS:
      return [...state, { ...action.user }];
    default:
      return state;
  }
}
