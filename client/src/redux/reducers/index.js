import { combineReducers } from 'redux';
import user from './user-reducer';
import users from './users-reducer';
const appReducer = combineReducers({
  user,
  users
});

export default appReducer;
